START=$1
END=$2
HASH=$3
THREADS=$4
#####
num=$START
until (echo -n $num | sha512sum -c <(echo "$HASH -") 2>/dev/null | grep OK ) || [[ $num == $END ]] ; do \
    let num+=1
done
if [[ $num != $END ]]; then
echo "Code: $num"
fi
wait
