FIRST=$(shuf -i 1000000000000-9999999999999 -n 1)
START=$(( FIRST - 20000 ))
END=$(( FIRST + 20000 ))
NUM=$( shuf -i $START-$END -n 1 )
HASH=$(echo -n $NUM | sha512sum | egrep -o '.{128}')
echo $NUM
echo $START $END
echo $HASH
