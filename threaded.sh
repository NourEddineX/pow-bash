START=$1
END=$2
HASH=$3
THREADS=$4
##########
if [[ $THREADS == '' ]]; then
    THREADS=1
fi
##########
incr=$(( (END-START)/THREADS ))
job_list=()
tstart=$START
##########


trap ctrl_c SIGTERM SIGINT

ctrl_c() {
    for job in $job_list;
    do kill -9 $job
    done
}


echo "Using $THREADS thread(s)"
for thread in $(seq $THREADS); do \
    bash PoW.sh $tstart $((tstart+incr)) $HASH | grep 'Code' &
    tstart=$((tstart+incr))
    let job_list+=(job_$thread)
done
# do not remove wait
wait
